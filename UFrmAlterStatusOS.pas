unit UFrmAlterStatusOS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.DBCtrls, Vcl.Buttons, PngSpeedButton, JvExControls, JvDBLookup;

type
  TFrmAlterStatusOS = class(TForm)
    pClient: TPanel;
    pBottom: TPanel;
    btnFechar: TPngSpeedButton;
    btnSalvar: TPngSpeedButton;
    dsOsStatus: TDataSource;
    dsOsAtualiza: TDataSource;
    dbList: TJvDBLookupList;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFecharClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure dbListClick(Sender: TObject);
  private
    itemAtual:integer;
  public
    { Public declarations }
  end;

var
  FrmAlterStatusOS: TFrmAlterStatusOS;

implementation

{$R *.dfm}

uses UDtmPrincipal, UFrmPrincipal;

procedure TFrmAlterStatusOS.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmAlterStatusOS.btnSalvarClick(Sender: TObject);
var
  Resposta: Integer;
begin
  if (DtmPrincipal.cdsAtualizaOs.Active) and (DtmPrincipal.cdsAtualizaOs.State in [dsEdit, dsInsert]) then
    begin
    if (DtmPrincipal.cdsAtualizaOsID_STATUS.Value = 12) then
      begin
        Resposta := MessageDlg('Deseja fechar a OS? '+ #13 + 'Ao fazer isso sera bloqueada a edi��o posterior!' ,mtInformation, mbYesNoCancel, 0);
        case Resposta of
          mrYes:
            begin
              try
                DtmPrincipal.cdsAtualizaOs.Post;
                DtmPrincipal.cdsAtualizaOs.ApplyUpdates(0);
                DtmPrincipal.cdsOsInfo.Refresh;
                DtmPrincipal.cdsAtualizaOs.Close;
                DtmPrincipal.sdsAtualizaOs.Close;
                DtmPrincipal.cdsStatusOs.Close;
                DtmPrincipal.sdsStausOS.Close;
                FrmPrincipal.btnAlterarStatusOS.Enabled := FALSE;
                FrmPrincipal.btnEditarObs.Enabled := FALSE;
                FrmAlterStatusOS.Close;
              except
                on e:Exception do
                  ShowMessage(e.Message + ' AO SALVAR ');
              end;
            end;
          mrNo:
            begin
              DtmPrincipal.cdsAtualizaOs.Cancel;
            end;
        end;
      end
    else
      begin
        try
          DtmPrincipal.cdsAtualizaOs.Post;
          DtmPrincipal.cdsAtualizaOs.ApplyUpdates(0);
          DtmPrincipal.cdsOsInfo.Refresh;
          DtmPrincipal.cdsAtualizaOs.Close;
          DtmPrincipal.sdsAtualizaOs.Close;
          DtmPrincipal.cdsStatusOs.Close;
          DtmPrincipal.sdsStausOS.Close;
          FrmAlterStatusOS.Close;
        except
          on e:Exception do
          ShowMessage(e.Message + ' AO SALVAR ');
        end;
      end;
    end;
end;

procedure TFrmAlterStatusOS.dbListClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsAtualizaOs.Edit;
  except
    on e:Exception do
      ShowMessage('Erro on edit!' + e.Message);
  end;
end;

procedure TFrmAlterStatusOS.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Freeandnil(FrmAlterStatusOS);
end;

procedure TFrmAlterStatusOS.FormCreate(Sender: TObject);
begin
  DtmPrincipal.sdsStausOS.Open;
  DtmPrincipal.cdsStatusOs.Open;
  DtmPrincipal.cdsAtualizaOs.Close;
  DtmPrincipal.cdsAtualizaOs.Params.Items[0].Value := DtmPrincipal.cdsOsInfo.Params.Items[0].Value;
  DtmPrincipal.sdsAtualizaOS.Open;
  DtmPrincipal.cdsAtualizaOs.Open;
end;

end.
