unit UFrmCheckProgramas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons,
  PngSpeedButton, Data.DB, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.Menus;

type
  TFrmCheckProgramas = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    edtPrograma: TEdit;
    DBGrid1: TDBGrid;
    dsProgramas: TDataSource;
    btnFechar: TPngSpeedButton;
    btnImprimir: TPngSpeedButton;
    btnRemover: TPngSpeedButton;
    btnAdd: TPngSpeedButton;
    ppListaPronta: TPopupMenu;
    Arquitetura1: TMenuItem;
    Imagem1: TMenuItem;
    Vdeo1: TMenuItem;
    Downloaders1: TMenuItem;
    Extras1: TMenuItem;
    VersesEssenciais1: TMenuItem;
    Office1: TMenuItem;
    GravadorCDDVD1: TMenuItem;
    Java1: TMenuItem;
    InternetExplore1: TMenuItem;
    Antivrus1: TMenuItem;
    AutoCad1: TMenuItem;
    N3DStudioMax1: TMenuItem;
    Sketchup1: TMenuItem;
    Maya1: TMenuItem;
    Revit1: TMenuItem;
    Promob1: TMenuItem;
    Vray1: TMenuItem;
    N20091: TMenuItem;
    N20101: TMenuItem;
    N20121: TMenuItem;
    N20131: TMenuItem;
    N20141: TMenuItem;
    N20151: TMenuItem;
    N20161: TMenuItem;
    Corel1: TMenuItem;
    Adobe1: TMenuItem;
    Gimp1: TMenuItem;
    Illustrator1: TMenuItem;
    Photoshop1: TMenuItem;
    inDesign1: TMenuItem;
    AdobePremier1: TMenuItem;
    SonyVegas1: TMenuItem;
    AtubeCatcher1: TMenuItem;
    Ares1: TMenuItem;
    Orbit1: TMenuItem;
    utorrent1: TMenuItem;
    N20031: TMenuItem;
    N20102: TMenuItem;
    N20132: TMenuItem;
    N20162: TMenuItem;
    Ashampoo1: TMenuItem;
    Nero1: TMenuItem;
    N7u551: TMenuItem;
    N8u741: TMenuItem;
    N8u771: TMenuItem;
    N8u901: TMenuItem;
    N8u921: TMenuItem;
    Verso81: TMenuItem;
    Verso91: TMenuItem;
    Verso101: TMenuItem;
    Verso111: TMenuItem;
    AvastFree1: TMenuItem;
    AviraFree1: TMenuItem;
    AVGFree1: TMenuItem;
    MicrosoftSecurity1: TMenuItem;
    rendMicro1: TMenuItem;
    Panda1: TMenuItem;
    KasperSky1: TMenuItem;
    Norton1: TMenuItem;
    Opera1: TMenuItem;
    PontoSecullum1: TMenuItem;
    X41: TMenuItem;
    X51: TMenuItem;
    X61: TMenuItem;
    X71: TMenuItem;
    X81: TMenuItem;
    procedure btnAddClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnRemoverClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnImprimirClick(Sender: TObject);
    procedure edtProgramaKeyPress(Sender: TObject; var Key: Char);
    procedure Vray1Click(Sender: TObject);
    procedure Promob1Click(Sender: TObject);
    procedure Revit1Click(Sender: TObject);
    procedure Maya1Click(Sender: TObject);
    procedure Sketchup1Click(Sender: TObject);
    procedure N3DStudioMax1Click(Sender: TObject);
    procedure N20091Click(Sender: TObject);
    procedure N20101Click(Sender: TObject);
    procedure N20121Click(Sender: TObject);
    procedure N20131Click(Sender: TObject);
    procedure N20141Click(Sender: TObject);
    procedure N20151Click(Sender: TObject);
    procedure N20161Click(Sender: TObject);
    procedure X81Click(Sender: TObject);
    procedure X71Click(Sender: TObject);
    procedure X61Click(Sender: TObject);
    procedure X51Click(Sender: TObject);
    procedure X41Click(Sender: TObject);
    procedure Illustrator1Click(Sender: TObject);
    procedure Photoshop1Click(Sender: TObject);
    procedure inDesign1Click(Sender: TObject);
    procedure Gimp1Click(Sender: TObject);
    procedure AdobePremier1Click(Sender: TObject);
    procedure SonyVegas1Click(Sender: TObject);
    procedure AtubeCatcher1Click(Sender: TObject);
    procedure Ares1Click(Sender: TObject);
    procedure Orbit1Click(Sender: TObject);
    procedure utorrent1Click(Sender: TObject);
    procedure Opera1Click(Sender: TObject);
    procedure PontoSecullum1Click(Sender: TObject);
    procedure N20031Click(Sender: TObject);
    procedure N20102Click(Sender: TObject);
    procedure N20132Click(Sender: TObject);
    procedure N20162Click(Sender: TObject);
    procedure Ashampoo1Click(Sender: TObject);
    procedure Nero1Click(Sender: TObject);
    procedure N7u551Click(Sender: TObject);
    procedure N8u741Click(Sender: TObject);
    procedure N8u771Click(Sender: TObject);
    procedure N8u901Click(Sender: TObject);
    procedure N8u921Click(Sender: TObject);
    procedure Verso81Click(Sender: TObject);
    procedure Verso91Click(Sender: TObject);
    procedure Verso101Click(Sender: TObject);
    procedure Verso111Click(Sender: TObject);
    procedure AvastFree1Click(Sender: TObject);
    procedure AviraFree1Click(Sender: TObject);
    procedure AVGFree1Click(Sender: TObject);
    procedure MicrosoftSecurity1Click(Sender: TObject);
    procedure rendMicro1Click(Sender: TObject);
    procedure Panda1Click(Sender: TObject);
    procedure KasperSky1Click(Sender: TObject);
    procedure Norton1Click(Sender: TObject);
  private
    idPrograma:Integer;
    procedure InsertProgram(programa:string);
  public
    { Public declarations }
  end;

var
  FrmCheckProgramas: TFrmCheckProgramas;

implementation

{$R *.dfm}

uses UDtmPrincipal;

procedure TFrmCheckProgramas.AdobePremier1Click(Sender: TObject);
begin
    InsertProgram('Adobe Premier');
end;

procedure TFrmCheckProgramas.Ares1Click(Sender: TObject);
begin
    InsertProgram('Ares');
end;

procedure TFrmCheckProgramas.Ashampoo1Click(Sender: TObject);
begin
    InsertProgram('Ashampoo');
end;

procedure TFrmCheckProgramas.AtubeCatcher1Click(Sender: TObject);
begin
    InsertProgram('Atube Catcher');
end;

procedure TFrmCheckProgramas.AvastFree1Click(Sender: TObject);
begin
    InsertProgram('Avast Free');
end;

procedure TFrmCheckProgramas.AVGFree1Click(Sender: TObject);
begin
    InsertProgram('AVG Free');
end;

procedure TFrmCheckProgramas.AviraFree1Click(Sender: TObject);
begin
    InsertProgram('Avira Free');
end;

procedure TFrmCheckProgramas.btnAddClick(Sender: TObject);
begin
  if edtPrograma.Text <> '' then
    begin
      InsertProgram(edtPrograma.Text);
      edtPrograma.Clear;
      edtPrograma.SetFocus;
    end;
end;

procedure TFrmCheckProgramas.btnFecharClick(Sender: TObject);
begin
  FrmCheckProgramas.Close;
end;

procedure TFrmCheckProgramas.btnImprimirClick(Sender: TObject);
begin
  if DtmPrincipal.cdsProgramas.RecordCount > 0 then
    begin
  DtmPrincipal.rvProgramas.Execute;
  FrmCheckProgramas.Close;
    end
  else
    MessageDlg('� necess�rio adicionar um programa a lista antes de imprimir!',mtWarning,[mbok],0)
end;

procedure TFrmCheckProgramas.btnRemoverClick(Sender: TObject);
begin
  if DtmPrincipal.cdsProgramas.RecordCount > 0 then
    try
      DtmPrincipal.cdsProgramas.Delete;
    except
      on E: Exception do
        MessageDlg('Erro', mtError, [mbOk],0);
    end
  else
    ShowMessage('N�o h� itens para remover!');
end;

procedure TFrmCheckProgramas.edtProgramaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    btnAdd.Click;
end;

procedure TFrmCheckProgramas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DtmPrincipal.cdsProgramas.EmptyDataSet;
  FreeAndNil(FrmCheckProgramas);
end;

procedure TFrmCheckProgramas.FormShow(Sender: TObject);
begin
  idPrograma := 1;
  try
  DtmPrincipal.cdsProgramas.EmptyDataSet;
  edtPrograma.SetFocus;
  except
    on E: Exception do
      MessageDlg('Erro ao criar tempor�rio', mtError, [mbOK], 0);
  end;
end;

procedure TFrmCheckProgramas.Gimp1Click(Sender: TObject);
begin
      InsertProgram('Gimp');
end;

procedure TFrmCheckProgramas.Illustrator1Click(Sender: TObject);
begin
    InsertProgram('Adobe Illustrator');
end;

procedure TFrmCheckProgramas.inDesign1Click(Sender: TObject);
begin
    InsertProgram('Adobe inDesign');
end;

procedure TFrmCheckProgramas.InsertProgram(programa: string);
begin
      DtmPrincipal.cdsProgramas.Append;
      DtmPrincipal.cdsProgramasifPrograma.AsInteger := idPrograma;
      DtmPrincipal.cdsProgramasNomePrograma.AsString := programa;
      DtmPrincipal.cdsProgramas.Post;
      Inc(idPrograma,1);
end;

procedure TFrmCheckProgramas.KasperSky1Click(Sender: TObject);
begin
    InsertProgram('Kaspersky');
end;

procedure TFrmCheckProgramas.Maya1Click(Sender: TObject);
begin
  InsertProgram('Maya');
end;

procedure TFrmCheckProgramas.MicrosoftSecurity1Click(Sender: TObject);
begin
    InsertProgram('Microsoft Security');
end;

procedure TFrmCheckProgramas.N20031Click(Sender: TObject);
begin
    InsertProgram('Microsoft Office 2003');
end;

procedure TFrmCheckProgramas.N20091Click(Sender: TObject);
begin
    InsertProgram('AutoCad 2009');
end;

procedure TFrmCheckProgramas.N20101Click(Sender: TObject);
begin
    InsertProgram('AutoCad 2010');
end;

procedure TFrmCheckProgramas.N20102Click(Sender: TObject);
begin
    InsertProgram('Microsoft Office 2010');
end;

procedure TFrmCheckProgramas.N20121Click(Sender: TObject);
begin
    InsertProgram('AutoCad 2012');
end;

procedure TFrmCheckProgramas.N20131Click(Sender: TObject);
begin
    InsertProgram('AutoCad 2013');
end;

procedure TFrmCheckProgramas.N20132Click(Sender: TObject);
begin
    InsertProgram('Microsoft Office 2013');
end;

procedure TFrmCheckProgramas.N20141Click(Sender: TObject);
begin
    InsertProgram('AutoCad 2014');
end;

procedure TFrmCheckProgramas.N20151Click(Sender: TObject);
begin
    InsertProgram('AutoCad 2015');
end;

procedure TFrmCheckProgramas.N20161Click(Sender: TObject);
begin
    InsertProgram('AutoCad 2016');
end;

procedure TFrmCheckProgramas.N20162Click(Sender: TObject);
begin
    InsertProgram('Microsoft Office 2016');
end;

procedure TFrmCheckProgramas.N3DStudioMax1Click(Sender: TObject);
begin
  InsertProgram('3D Studio Max');
end;

procedure TFrmCheckProgramas.N7u551Click(Sender: TObject);
begin
    InsertProgram('Java 7 update 55');
end;

procedure TFrmCheckProgramas.N8u741Click(Sender: TObject);
begin
    InsertProgram('Java 8 update 74');
end;

procedure TFrmCheckProgramas.N8u771Click(Sender: TObject);
begin
    InsertProgram('Java 8 update 77');
end;

procedure TFrmCheckProgramas.N8u901Click(Sender: TObject);
begin
    InsertProgram('Java 8 update 90');
end;

procedure TFrmCheckProgramas.N8u921Click(Sender: TObject);
begin
    InsertProgram('Java 8 update 92');
end;

procedure TFrmCheckProgramas.Nero1Click(Sender: TObject);
begin
    InsertProgram('Nero');
end;

procedure TFrmCheckProgramas.Norton1Click(Sender: TObject);
begin
    InsertProgram('Norton');
end;

procedure TFrmCheckProgramas.Opera1Click(Sender: TObject);
begin
    InsertProgram('Compufour Clipp');
end;

procedure TFrmCheckProgramas.Orbit1Click(Sender: TObject);
begin
    InsertProgram('Orbit');
end;

procedure TFrmCheckProgramas.Panda1Click(Sender: TObject);
begin
    InsertProgram('Panda');
end;

procedure TFrmCheckProgramas.Photoshop1Click(Sender: TObject);
begin
    InsertProgram('Adobe Photoshop');
end;

procedure TFrmCheckProgramas.PontoSecullum1Click(Sender: TObject);
begin
    InsertProgram('Ponto Secullum');
end;

procedure TFrmCheckProgramas.Promob1Click(Sender: TObject);
begin
  InsertProgram('Promob');
end;

procedure TFrmCheckProgramas.rendMicro1Click(Sender: TObject);
begin
    InsertProgram('TrendMicro');
end;

procedure TFrmCheckProgramas.Revit1Click(Sender: TObject);
begin
  InsertProgram('Solidworks');
end;

procedure TFrmCheckProgramas.Sketchup1Click(Sender: TObject);
begin
  InsertProgram('Sketchup');
end;

procedure TFrmCheckProgramas.SonyVegas1Click(Sender: TObject);
begin
    InsertProgram('Sony Vegas');
end;

procedure TFrmCheckProgramas.utorrent1Click(Sender: TObject);
begin
    InsertProgram('utorrent');
end;

procedure TFrmCheckProgramas.Verso101Click(Sender: TObject);
begin
    InsertProgram('Internet Explorer 10');
end;

procedure TFrmCheckProgramas.Verso111Click(Sender: TObject);
begin
    InsertProgram('Internet Explorer 11');
end;

procedure TFrmCheckProgramas.Verso81Click(Sender: TObject);
begin
    InsertProgram('Internet Explorer 8');
end;

procedure TFrmCheckProgramas.Verso91Click(Sender: TObject);
begin
    InsertProgram('Internet Explorer 9');
end;

procedure TFrmCheckProgramas.Vray1Click(Sender: TObject);
begin
  InsertProgram('Vray');
end;

procedure TFrmCheckProgramas.X41Click(Sender: TObject);
begin
    InsertProgram('Corel X4');
end;

procedure TFrmCheckProgramas.X51Click(Sender: TObject);
begin
    InsertProgram('Corel X5');
end;

procedure TFrmCheckProgramas.X61Click(Sender: TObject);
begin
    InsertProgram('Corel X6');
end;

procedure TFrmCheckProgramas.X71Click(Sender: TObject);
begin
    InsertProgram('Corel X7');
end;

procedure TFrmCheckProgramas.X81Click(Sender: TObject);
begin
    InsertProgram('Corel X8');
end;

end.
