object FrmConfiguracao: TFrmConfiguracao
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Selecionar Banco'
  ClientHeight = 149
  ClientWidth = 505
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pgConfig: TPageControl
    Left = 0
    Top = 0
    Width = 505
    Height = 108
    ActivePage = tbLocal
    Align = alClient
    TabOrder = 0
    object tbLocal: TTabSheet
      Caption = 'Conex'#227'o Local'
      object edtBancoLocal: TLabeledEdit
        Left = 96
        Top = 19
        Width = 353
        Height = 21
        EditLabel.Width = 80
        EditLabel.Height = 13
        EditLabel.Caption = 'Banco de dados:'
        LabelPosition = lpLeft
        TabOrder = 0
      end
      object btnBuscarRemoto: TButton
        Left = 374
        Top = 46
        Width = 75
        Height = 25
        Caption = 'Buscar'
        TabOrder = 1
      end
      object btnTestarLocal: TButton
        Left = 293
        Top = 46
        Width = 75
        Height = 25
        Caption = 'Testar'
        Enabled = False
        TabOrder = 2
      end
    end
    object tbRemoto: TTabSheet
      Caption = 'Conex'#227'o Remoto'
      ImageIndex = 1
      object btnTestarRemoto: TButton
        Left = 374
        Top = 46
        Width = 75
        Height = 25
        Caption = 'Testar'
        Enabled = False
        TabOrder = 0
      end
      object edtBancoRemoto: TLabeledEdit
        Left = 96
        Top = 19
        Width = 353
        Height = 21
        EditLabel.Width = 80
        EditLabel.Height = 13
        EditLabel.Caption = 'Banco de dados:'
        LabelPosition = lpLeft
        TabOrder = 1
      end
      object edtServidorRemoto: TLabeledEdit
        Left = 96
        Top = 46
        Width = 153
        Height = 21
        EditLabel.Width = 44
        EditLabel.Height = 13
        EditLabel.Caption = 'Servidor:'
        LabelPosition = lpLeft
        TabOrder = 2
      end
      object edtPortaRemoto: TLabeledEdit
        Left = 287
        Top = 46
        Width = 81
        Height = 21
        EditLabel.Width = 30
        EditLabel.Height = 13
        EditLabel.Caption = 'Porta:'
        LabelPosition = lpLeft
        TabOrder = 3
      end
    end
  end
  object pbottom: TPanel
    Left = 0
    Top = 108
    Width = 505
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object btnFechar: TButton
      Left = 0
      Top = 0
      Width = 89
      Height = 41
      Align = alLeft
      Caption = 'Fechar'
      TabOrder = 0
    end
    object btnSalvar: TButton
      Left = 416
      Top = 0
      Width = 89
      Height = 41
      Align = alRight
      Caption = 'Salvar'
      TabOrder = 1
    end
  end
  object odLocal: TOpenDialog
    DefaultExt = '*.FDB'
    FileName = 'CLIPP.FDB'
    InitialDir = 'c:\'
    Left = 240
    Top = 72
  end
end
