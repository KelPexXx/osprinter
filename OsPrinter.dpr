program OsPrinter;

uses
  Vcl.Forms,
  UFrmPrincipal in 'UFrmPrincipal.pas' {FrmPrincipal},
  UDtmPrincipal in 'UDtmPrincipal.pas' {DtmPrincipal: TDataModule},
  Vcl.Themes,
  Vcl.Styles,
  UFrmListaOS in 'UFrmListaOS.pas' {FrmListaOS},
  UFrmConfigImpressora in 'UFrmConfigImpressora.pas' {FrmConfigImpressora},
  UFrmConfigBanco in 'UFrmConfigBanco.pas' {FrmConfigBanco},
  UFrmCheckProgramas in 'UFrmCheckProgramas.pas' {FrmCheckProgramas},
  UFrmCheckFormatCBkp in 'UFrmCheckFormatCBkp.pas' {FrmCheckFormatCBkp},
  UFrmEditObs in 'UFrmEditObs.pas' {FrmEditObs},
  UFrmAlterStatusOS in 'UFrmAlterStatusOS.pas' {FrmAlterStatusOS},
  UFrmConfText in 'UFrmConfText.pas' {FrmConfText};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Light');
  Application.Title := 'OSPrinter';
  application.ShowMainForm := FALSE;
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.CreateForm(TFrmConfText, FrmConfText);
  Application.Run;
end.
