unit UFrmConfigBanco;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons,
  PngSpeedButton, inifiles, Vcl.Imaging.pngimage;

type
  TFrmConfigBanco = class(TForm)
    pBottom: TPanel;
    pClient: TPanel;
    btnSalvar: TPngSpeedButton;
    btnFechar: TPngSpeedButton;
    edtDB: TLabeledEdit;
    lblInfo: TLabel;
    lblInst: TLabel;
    imgSide: TImage;
    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure lblInstClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmConfigBanco: TFrmConfigBanco;

implementation

{$R *.dfm}

uses UDtmPrincipal, UFrmPrincipal;

procedure TFrmConfigBanco.FormShow(Sender: TObject);
begin
  edtDB.Text := FrmPrincipal.BASE;
end;

procedure TFrmConfigBanco.lblInstClick(Sender: TObject);
begin
  edtDB.Text := lblInst.Caption;
end;

procedure TFrmConfigBanco.btnSalvarClick(Sender: TObject);
  var
   Ini: TIniFile;
 begin
    FrmPrincipal.BASE := edtDB.Text;
    DtmPrincipal.Conexao.Params.Values['database'] := edtDB.Text;
    DtmPrincipal.Conexao.Open;
   if not DtmPrincipal.Conexao.Connected then
      MessageDlg('Erro!N�o foi poss�vel conectar ao banco de dados!',mtWarning,[mbOk],1)
   else
     begin
       Ini := TIniFile.Create( ChangeFileExt( Application.ExeName, '.INI' ));
       try
          Ini.WriteString('CONEXAO', 'DATABASE', FrmPrincipal.BASE);
       finally
         Ini.Free;
       end;

       Close;
     end;
end;

procedure TFrmConfigBanco.btnFecharClick(Sender: TObject);
begin
  Application.Terminate;
end;

end.
