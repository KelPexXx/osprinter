unit UFrmListaOS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Buttons, PngSpeedButton,
  Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls;

type
  TFrmListaOS = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    btnSelecionar: TPngSpeedButton;
    dsLista: TDataSource;
    btnAtualizar: TPngSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnSelecionarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmListaOS: TFrmListaOS;

implementation

{$R *.dfm}

uses UDtmPrincipal, UFrmPrincipal;

procedure TFrmListaOS.btnAtualizarClick(Sender: TObject);
begin
  dsLista.DataSet.Refresh;
  dsLista.DataSet.Refresh;
  dsLista.DataSet.Refresh;
end;

procedure TFrmListaOS.btnSelecionarClick(Sender: TObject);
begin
  FrmPrincipal.edtOSCOD.Text := DtmPrincipal.cdsListaOsID_OS.AsString;
  FrmPrincipal.btnBuscar1.Click;
  close;
end;

procedure TFrmListaOS.DBGrid1DblClick(Sender: TObject);
begin
  FrmPrincipal.edtOSCOD.Text := DtmPrincipal.cdsListaOsID_OS.AsString;
    FrmPrincipal.btnBuscar1.Click;
  close;
end;

procedure TFrmListaOS.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnSelecionar.Click;
end;

procedure TFrmListaOS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmListaOS);
end;

procedure TFrmListaOS.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  dsLista.DataSet.Close;
end;

procedure TFrmListaOS.FormShow(Sender: TObject);
begin
  dsLista.DataSet.Open;
  FrmListaOS.Width := Screen.Width -Trunc(((Screen.Width/10)*2));
end;



end.
