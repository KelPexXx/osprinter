unit UFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  System.UITypes, System.Types, Forms, Data.DB, Vcl.Grids,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Controls, Vcl.DBCtrls, Dialogs,
  Vcl.Imaging.pngimage, Vcl.DBGrids, Vcl.Mask, IniFiles, Printers, RpDevice,
  Vcl.ImgList, PngImageList, Vcl.AppEvnts, Vcl.Menus, Vcl.Buttons, MIDASLIB,
  PngSpeedButton;
type
  TFrmPrincipal = class(TForm)
    dsOsItem: TDataSource;
    dsOSInfo: TDataSource;
    pAll: TPanel;
    pTop: TPanel;
    gbItens: TGroupBox;
    dgItens: TDBGrid;
    lblNumOs: TLabel;
    edtOSCOD: TEdit;
    ppMenuIcBandeija: TPopupMenu;
    Abrir1: TMenuItem;
    ImprimirOs1: TMenuItem;
    ImprUltOS: TMenuItem;
    Fechar1: TMenuItem;
    btnBuscar1: TPngSpeedButton;
    TrayIcon: TTrayIcon;
    edtUltOs: TLabeledEdit;
    btnSelctUltOS: TPngSpeedButton;
    imgMenuList: TPngImageList;
    ppMenuForm: TPopupMenu;
    ConfigurarImpressora1: TMenuItem;
    ConfigurarBanco1: TMenuItem;
    btnChecklists: TButton;
    ppMenuChecklists: TPopupMenu;
    FormatacocBackup1: TMenuItem;
    FormatacosemBackup1: TMenuItem;
    InstalaodeProgramas1: TMenuItem;
    OramentoComputador1: TMenuItem;
    OramentoNotebook1: TMenuItem;
    Checklists1: TMenuItem;
    FormataocomBackup1: TMenuItem;
    FormataosemBackup1: TMenuItem;
    InstalaodeProgramas2: TMenuItem;
    OramentodeNotebook1: TMenuItem;
    OramentodeComputador1: TMenuItem;
    Panel1: TPanel;
    gbObsOs: TGroupBox;
    dbObsOS: TDBMemo;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    dbAtendente: TDBText;
    dbprevEntrega: TDBText;
    dbGarantia: TDBText;
    dbData: TDBText;
    dbHora: TDBText;
    dbDavAtual: TDBText;
    dbDavAbertura: TDBText;
    dbCliente: TDBText;
    dbFantasia: TDBText;
    dbSolicitante: TDBText;
    lblEnd: TLabel;
    dbCep: TDBText;
    lblComercial: TLabel;
    lblResidencial: TLabel;
    dbEmail: TDBText;
    dbEmail2: TDBText;
    lblCelular: TLabel;
    lblCidadeEstado: TLabel;
    dbBairro: TDBText;
    dbIm: TDBText;
    dbRGIE: TDBText;
    dbCnpjCpf: TDBText;
    dbTecnico: TDBText;
    dbObjeto: TDBText;
    dbMarca: TDBText;
    dbSenha: TDBText;
    dbAcessorios: TDBText;
    dbEntrega: TDBText;
    dbObs: TDBText;
    dbDefeito: TDBText;
    dbTotalProd: TDBText;
    dbTotalServ: TDBText;
    dbSubTotal: TDBText;
    dbFrete: TDBText;
    dbTotal: TDBText;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label9: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    ppObsOrdem: TPopupMenu;
    EditarObservao1: TMenuItem;
    AlterarEstado1: TMenuItem;
    Panel6: TPanel;
    btnAlterarStatusOS: TPngSpeedButton;
    btnEditarObs: TPngSpeedButton;
    dbStatusOs: TDBText;
    Label38: TLabel;
    btnImprimir: TPngSpeedButton;
    btnPrxOS: TPngSpeedButton;
    btnAntOS: TPngSpeedButton;
    procedure btnBuscarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure edtOSCODKeyPress(Sender: TObject; var Key: Char);
    procedure edtOSCODEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Abrir1Click(Sender: TObject);
    procedure ImprimirOs1Click(Sender: TObject);
    procedure TrayIconDblClick(Sender: TObject);
    procedure edtUltOsSubLabelClick(Sender: TObject);
    procedure edtUltOsClick(Sender: TObject);
    procedure edtUltOsEnter(Sender: TObject);
    procedure btnSelctUltOSClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ConfigurarImpressora1Click(Sender: TObject);
    procedure ImprUltOSClick(Sender: TObject);
    procedure ConfigurarBanco1Click(Sender: TObject);
    procedure InstalaodeProgramas1Click(Sender: TObject);
    procedure gbObsOsDblClick(Sender: TObject);
    procedure btnAlterarStatusOSClick(Sender: TObject);
    procedure btnEditarObsClick(Sender: TObject);
    procedure btnPrxOSClick(Sender: TObject);
    procedure btnAntOSClick(Sender: TObject);
  private
    procedure AtualizaUltOS;
    procedure ClearForm;
  public
     BASE:String;
     sImpressora,sCamOsPadra,sCamFormatSBkp,sCamFormatCBkp,sCamFormatGold:String;
     aStatusOs:Array of string;
     procedure GetListStatus;
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

uses UDtmPrincipal, UFrmListaOS, UFrmConfigImpressora, UFrmConfigBanco,
  UFrmCheckProgramas, UFrmEditObs, UFrmAlterStatusOS;

procedure TFrmPrincipal.Abrir1Click(Sender: TObject);
begin

  FrmPrincipal.Show;
end;

procedure TFrmPrincipal.AtualizaUltOS;
var
  flag:boolean;
begin
  if DtmPrincipal.Conexao.Connected then
    flag := FALSE
  else
    begin
      DtmPrincipal.Conexao.Open;
      flag := TRUE;
    end;
  DtmPrincipal.sdsUltOs.Open;
  DtmPrincipal.sdsUltOs.Refresh;
  edtUltOs.Text :=  DtmPrincipal.sdsUltOsID_OS.AsString;
  DtmPrincipal.sdsUltOs.close;
  if flag then
    DtmPrincipal.Conexao.Close;
end;

procedure TFrmPrincipal.btnAlterarStatusOSClick(Sender: TObject);
begin
  if (DtmPrincipal.cdsAtualizaOsID_STATUS.Value = 12) then
    begin
      MessageDlg('Aten��o! Esta OS esta fechada, n�o pode ser editado o seu estado ou observa��o!',mtWarning,[mbOk],0);
    end
  else
    begin
      Application.CreateForm(TFrmAlterStatusOS,FrmAlterStatusOS);
      FrmAlterStatusOS.Show;
    end;
end;

procedure TFrmPrincipal.btnAntOSClick(Sender: TObject);
var
  i:integer;
begin
  if edtOSCOD.Text <> '' then
    begin
      edtOSCOD.Text := inttostr(strtoint(edtOSCOD.Text) - 1);
      btnBuscar1.Click;
    end
  else
    btnSelctUltOS.Click;
end;

procedure TFrmPrincipal.btnBuscarClick(Sender: TObject);
begin
  if edtOSCOD.Text <> '' then
  begin
    DtmPrincipal.Conexao.Open;
    DtmPrincipal.cdsOsInfo.Close;
    DtmPrincipal.cdsOsInfo.Params.Items[0].Value := StrToInt(edtOSCOD.Text);
    DtmPrincipal.cdsOsInfo.Open;
    DtmPrincipal.cdsOsItem.Close;
    DtmPrincipal.cdsOsItem.Params.Items[0].Value := StrToInt(edtOSCOD.Text);
    DtmPrincipal.cdsOsItem.Open;

    if DtmPrincipal.cdsOsInfo.RecordCount = 1 then
      begin
        btnImprimir.Enabled := TRUE;
        with DtmPrincipal do
          begin
            lblEnd.Caption := cdsOsInfoEND_TIPO.asstring + ' ' + cdsOsInfoEND_LOGRAD.AsString + ', ' + cdsOsInfoEND_NUMERO.AsString;
            lblCidadeEstado.Caption := cdsOsInfoCIDADE.AsString + ', ' + cdsOsInfoUF.AsString;
            lblComercial.Caption := cdsOsInfoDDD_COMER.AsString + ' ' + cdsOsInfoFONE_COMER.AsString;
            lblResidencial.Caption := cdsOsInfoDDD_RESID.AsString + ' ' + cdsOsInfoFONE_RESID.AsString;
            lblCelular.Caption := cdsOsInfoDDD_CELUL.AsString + ' ' + cdsOsInfoFONE_CELUL.AsString;
          end;
        if (DtmPrincipal.cdsOsInfoID_STATUS.Value <> 12) then
          begin
            btnAlterarStatusOS.Enabled := TRUE;
            btnEditarObs.Enabled := TRUE;
          end
        else
          begin
            btnAlterarStatusOS.Enabled := FALSE;
            btnEditarObs.Enabled := FALSE;
          end;
      end
    else
      begin
        ShowMEssage('Erro! OS n�o encontrada!');
        edtOSCOD.SetFocus;
        btnImprimir.Enabled := FALSE;
        btnAlterarStatusOS.Enabled := FALSE;
        btnEditarObs.Enabled := FALSE;
      end;
  end
else
  begin
    Application.CreateForm(TFrmListaOS,FrmListaOS);
    FrmListaOS.Show;
  end;
end;

procedure TFrmPrincipal.btnEditarObsClick(Sender: TObject);
begin
  Application.CreateForm(TFrmEditObs,FrmEditObs);
  FrmEditObs.Show;
end;

procedure TFrmPrincipal.btnImprimirClick(Sender: TObject);
begin
  DtmPrincipal.rvOrdemCupom.Execute;
  DtmPrincipal.Conexao.CloseDataSets;
  DtmPrincipal.Conexao.Close;
end;

procedure TFrmPrincipal.btnPrxOSClick(Sender: TObject);
var
  i:integer;
begin
  if edtOSCOD.Text <> '' then
    begin
      edtOSCOD.Text := inttostr(strtoint(edtOSCOD.Text) + 1);
      btnBuscar1.Click;
    end
  else
    btnSelctUltOS.Click;
end;

procedure TFrmPrincipal.btnSelctUltOSClick(Sender: TObject);
begin
  edtOSCOD.Text := edtUltOs.Text;
  btnBuscar1.Click;
end;

procedure TFrmPrincipal.ClearForm;
begin
  lblEnd.Caption := '';
  lblComercial.Caption := '';
  lblResidencial.Caption := '';
  lblCelular.Caption := '';
end;

procedure TFrmPrincipal.ConfigurarBanco1Click(Sender: TObject);
begin
  if not assigned(FrmConfigBanco) then
    begin
      Application.CreateForm(TFrmConfigBanco,FrmConfigBanco);
    end;
  FrmConfigBanco.Show;
end;

procedure TFrmPrincipal.ConfigurarImpressora1Click(Sender: TObject);
begin
  if not assigned(FrmConfigImpressora) then
    begin
      Application.CreateForm(TFrmConfigImpressora,FrmConfigImpressora);
    end;
  FrmConfigImpressora.Show;
end;

procedure TFrmPrincipal.edtOSCODEnter(Sender: TObject);
begin
  btnImprimir.Enabled := FALSE;
end;

procedure TFrmPrincipal.edtOSCODKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnBuscarClick(sender);
end;

procedure TFrmPrincipal.edtUltOsClick(Sender: TObject);
begin
 AtualizaUltOS
end;

procedure TFrmPrincipal.edtUltOsEnter(Sender: TObject);
begin
  AtualizaUltOS;
end;

procedure TFrmPrincipal.edtUltOsSubLabelClick(Sender: TObject);
begin
  AtualizaUltOS;
end;

procedure TFrmPrincipal.Fechar1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFrmPrincipal.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  canclose := FALSE;
  DtmPrincipal.cdsOsItem.Close;
  DtmPrincipal.sdsOsItem.Close;
  DtmPrincipal.cdsOsInfo.Close;
  DtmPrincipal.sdsOsInfo.Close;
  DtmPrincipal.Conexao.Close;
  FrmPrincipal.Hide;
  ClearForm;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create( ChangeFileExt( Application.ExeName, '.INI' ));
  try
    BASE  := Ini.ReadString('CONEXAO', 'DATABASE', '');
    sImpressora := Ini.ReadString('IMPRESSORA','TERMICA','');
    sCamOsPadra := Ini.ReadString('CONFIGURACOES','TEXTOPADRAO','');
    sCamFormatSBkp := Ini.ReadString('CONFIGURACOES','TEXTOFORMATSBKP','');
    sCamFormatCBkp := Ini.ReadString('CONFIGURACOES','TEXTOFORMATCBKP','');
    sCamFormatGold := Ini.ReadString('CONFIGURACOES','TEXTOFORMATGOLD','');
  finally
    Ini.Free;
  end;
  Application.CreateForm(TDtmPrincipal,DtmPrincipal);
  try
    DtmPrincipal.Conexao.Open;
    if not(DtmPrincipal.Conexao.Connected) then
      begin
        MessageDlg('Erro! N�o conectado!',mtWarning,[mbOk],0);
        Application.CreateForm(TFrmConfigBanco,FrmConfigBanco);
        FrmConfigBanco.Show;
      end
    else
    begin
      GetListStatus;
    end;
  finally
    DtmPrincipal.Conexao.Close;
  end;
  if not rpdev.SelectPrinter(sImpressora,false) then
    begin
      Showmessage('N�o foi poss�vel encontrar a impressora!');
      Application.CreateForm(TFrmConfigImpressora,FrmConfigImpressora);
      FrmConfigImpressora.Show;
    end;
end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
begin
  edtOSCOD.Clear;
  edtOSCOD.SetFocus;
  AtualizaUltOS;
end;

procedure TFrmPrincipal.gbObsOsDblClick(Sender: TObject);
begin
  Application.CreateForm(TFrmEditObs,FrmEditObs);
  FrmEditObs.Show;
end;

procedure TFrmPrincipal.GetListStatus;
var
  i,count:integer;
begin
  DtmPrincipal.sdsStausOS.Open;
  count := DtmPrincipal.sdsStausOS.RecordCount;
  DtmPrincipal.sdsStausOS.First;
  while not DtmPrincipal.sdsStausOS.Eof do
    begin
      if DtmPrincipal.sdsStausOSSTATUS.AsString = 'A' then
        begin
          SetLength(aStatusOs,Length(aStatusOs)+1);
          aStatusOs[Length(aStatusOs)-1] := DtmPrincipal.sdsStausOSDESCRICAO.AsString;
        end;
      DtmPrincipal.sdsStausOS.Next;
    end;
  DtmPrincipal.sdsStausOS.Close;

end;

procedure TFrmPrincipal.ImprimirOs1Click(Sender: TObject);
var
  codOS_ID:String;
begin
  codOS_ID := InputBox('Imprimir OS','C�digo: ','');
  edtOSCOD.Text := codOS_ID;
  btnBuscar1.Click;
  DtmPrincipal.rvOrdemCupom.Execute;
  DtmPrincipal.cdsOsItem.Close;
  DtmPrincipal.sdsOsItem.Close;
  DtmPrincipal.cdsOsInfo.Close;
  DtmPrincipal.sdsOsInfo.Close;
  DtmPrincipal.Conexao.Close;
  ClearForm;

end;

procedure TFrmPrincipal.ImprUltOSClick(Sender: TObject);
begin
  AtualizaUltOS;
  edtOSCOD.Text := edtUltOs.Text;
  btnBuscar1.Click;
  DtmPrincipal.rvOrdemCupom.Execute;
  DtmPrincipal.cdsOsItem.Close;
  DtmPrincipal.sdsOsItem.Close;
  DtmPrincipal.cdsOsInfo.Close;
  DtmPrincipal.sdsOsInfo.Close;
  DtmPrincipal.Conexao.Close;
  ClearForm;
end;

procedure TFrmPrincipal.InstalaodeProgramas1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmCheckProgramas,FrmCheckProgramas);
  FrmCheckProgramas.Show;
end;

procedure TFrmPrincipal.TrayIconDblClick(Sender: TObject);
begin
  FrmPrincipal.Show;
end;

end.
