unit UFrmEditObs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, PngSpeedButton,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.DBCtrls, Data.DB, Vcl.Menus;

type
  TFrmEditObs = class(TForm)
    pBottom: TPanel;
    btnSalvar: TPngSpeedButton;
    btnFechar: TPngSpeedButton;
    btnLimparOBS: TPngSpeedButton;
    btnTextoPadrao: TPngSpeedButton;
    dsForm: TDataSource;
    ppTextoPad: TPopupMenu;
    AddTextoOSPadro1: TMenuItem;
    AddTextoFormataoPadro1: TMenuItem;
    AddTextoFormataoCBackupPadro1: TMenuItem;
    AddTextoFormataoCBackupGold1: TMenuItem;
    redtOBS: TDBMemo;
    procedure btnFecharClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnTextoPadraoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure redtOBSClick(Sender: TObject);
    procedure btnLimparOBSClick(Sender: TObject);
  private
    procedure insereTexto(Texto:string);
    procedure alteraBotoes(status:boolean);
  public
    { Public declarations }
  end;

var
  FrmEditObs: TFrmEditObs;

implementation

{$R *.dfm}

uses UDtmPrincipal, UFrmAlterStatusOS;

procedure TFrmEditObs.alteraBotoes(status: boolean);
begin
  btnSalvar.Enabled := Status;
  btnLimparOBS.Enabled := Status;
  btnTextoPadrao.Enabled := Status;
end;

procedure TFrmEditObs.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmEditObs.btnLimparOBSClick(Sender: TObject);
begin
  insereTexto(' ');
end;

procedure TFrmEditObs.btnSalvarClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsAtualizaOs.Post;
    DtmPrincipal.cdsAtualizaOs.ApplyUpdates(0);
    DtmPrincipal.cdsOsInfo.Refresh;
    DtmPrincipal.cdsAtualizaOs.Close;
    DtmPrincipal.sdsAtualizaOs.Close;
    FrmEditObs.Close;
  except
    on e:Exception do
      ShowMessage(e.Message + ' AO SALVAR ');
  end;
end;

procedure TFrmEditObs.btnTextoPadraoClick(Sender: TObject);
begin
  btnTextoPadrao.PopupMenu.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;

procedure TFrmEditObs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Freeandnil(FrmEditObs);
end;

procedure TFrmEditObs.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  Resposta: Integer;
begin
  if (DtmPrincipal.cdsAtualizaOs.Active) and (DtmPrincipal.cdsAtualizaOs.State in [dsEdit, dsInsert]) then
    begin
      Resposta := MessageDlg('Deseja Salvar as alterações antes de sair?',mtInformation, mbYesNoCancel, 0);
      case Resposta of
        mrYes:
          begin
            try
              DtmPrincipal.cdsAtualizaOs.Post;
              DtmPrincipal.cdsAtualizaOs.ApplyUpdates(0);
              DtmPrincipal.cdsOsInfo.Refresh;
            except
              on e:Exception do
                ShowMessage(e.Message + ' AO SALVAR ');
            end;
            CanClose := True;
          end;
        mrNo:
          begin
            DtmPrincipal.cdsAtualizaOs.Cancel;
            CanCLose := True;
          end;
        else
          begin
            CanClose := False;
            exit;
          end;
      end;
    end;
end;

procedure TFrmEditObs.FormCreate(Sender: TObject);
begin
  DtmPrincipal.cdsAtualizaOs.Close;
  DtmPrincipal.cdsAtualizaOs.Params.Items[0].Value := DtmPrincipal.cdsOsInfo.Params.Items[0].Value;
  DtmPrincipal.sdsAtualizaOS.Open;
  DtmPrincipal.cdsAtualizaOs.Open;
end;

procedure TFrmEditObs.insereTexto(Texto: string);
begin
  if not (DtmPrincipal.cdsAtualizaOs.State in [dsEdit, dsInsert]) then
    begin
      DtmPrincipal.cdsAtualizaOs.Edit;
    end;
  DtmPrincipal.cdsAtualizaOsOBSERVACAO.Value := Texto;
end;

procedure TFrmEditObs.redtOBSClick(Sender: TObject);
begin
  if not (DtmPrincipal.cdsAtualizaOs.State in [dsEdit, dsInsert]) then
    begin
      DtmPrincipal.cdsAtualizaOs.Edit;
      alteraBotoes(TRUE);
    end;
end;

end.
