unit UFrmConfigImpressora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls, Vcl.ExtCtrls, Printers, inifiles,RpDevice, Vcl.Imaging.pngimage;

type
  TFrmConfigImpressora = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    ComboBox1: TComboBox;
    PngSpeedButton1: TPngSpeedButton;
    PngSpeedButton2: TPngSpeedButton;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure PngSpeedButton2Click(Sender: TObject);
    procedure PngSpeedButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    function BuscaImpressora:integer;
  public
    { Public declarations }
  end;

var
  FrmConfigImpressora: TFrmConfigImpressora;

implementation

{$R *.dfm}

uses UFrmPrincipal;

function TFrmConfigImpressora.BuscaImpressora: integer;
var
  I: Integer;
begin
  result := 0;
  for I := 0 to ComboBox1.Items.Count -1 do
    begin
      if ComboBox1.Items.Strings[I] = FrmPrincipal.sImpressora then
        result := I;
    end;
end;

procedure TFrmConfigImpressora.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil(FrmConfigImpressora);
end;

procedure TFrmConfigImpressora.FormCreate(Sender: TObject);
begin
  ComboBox1.Items := Printer.Printers;
  ComboBox1.ItemIndex := BuscaImpressora;
end;

procedure TFrmConfigImpressora.PngSpeedButton1Click(Sender: TObject);
  var
   Ini: TIniFile;
 begin
   FrmPrincipal.sImpressora := ComboBox1.Items.Strings[ComboBox1.ItemIndex];
   if not rpdev.SelectPrinter(FrmPrincipal.sImpressora,false) then
      MessageDlg('Erro! N�o foi poss�vel utilizar esta impressora!',mtWarning,[mbOk],1)
   else
     begin
       Ini := TIniFile.Create( ChangeFileExt( Application.ExeName, '.INI' ));
       try
          Ini.WriteString('IMPRESSORA','TERMICA',FrmPrincipal.sImpressora);
       finally
         Ini.Free;
       end;

       Close;
     end;
end;

procedure TFrmConfigImpressora.PngSpeedButton2Click(Sender: TObject);
begin
 Application.Terminate;
end;

end.
