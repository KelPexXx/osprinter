unit UFrmConfText;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Imaging.pngimage,inifiles;

type
  TFrmConfText = class(TForm)
    pClient: TPanel;
    imgSide: TImage;
    edtCamOSPad: TLabeledEdit;
    pBottom: TPanel;
    btnSalvar: TPngSpeedButton;
    PngSpeedButton2: TPngSpeedButton;
    edtCamFormatSBkp: TLabeledEdit;
    edtCamFormatCBkp: TLabeledEdit;
    edtCamFormatGold: TLabeledEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmConfText: TFrmConfText;

implementation

{$R *.dfm}

end.
