unit UFrmConfiguracao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFrmConfiguracao = class(TForm)
    pgConfig: TPageControl;
    tbLocal: TTabSheet;
    tbRemoto: TTabSheet;
    edtBancoLocal: TLabeledEdit;
    btnBuscarRemoto: TButton;
    pbottom: TPanel;
    btnFechar: TButton;
    btnSalvar: TButton;
    btnTestarLocal: TButton;
    btnTestarRemoto: TButton;
    edtBancoRemoto: TLabeledEdit;
    odLocal: TOpenDialog;
    edtServidorRemoto: TLabeledEdit;
    edtPortaRemoto: TLabeledEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmConfiguracao: TFrmConfiguracao;

implementation

{$R *.dfm}

uses UFrmPrincipal, UDtmPrincipal;

end.
